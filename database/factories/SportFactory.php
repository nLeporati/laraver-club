<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Sport::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence(3),
        'creator_id' => 1,
    ];
});
