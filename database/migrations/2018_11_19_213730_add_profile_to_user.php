<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfileToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {            
            $table->string('run')->nullable();
            $table->string('lastname');
            $table->string('cellnumber')->nullable();
            $table->string('image')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('origin')->nullable();
            $table->string('nacionality')->nullable();
            $table->string('origin_club')->nullable();
            $table->string('status')->default('to-active');
            $table->string('role')->default('guest');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('run');
            $table->dropColumn('lastname');
            $table->dropColumn('cellnumber');
            $table->dropColumn('image');
            $table->dropColumn('birthdate');
            $table->dropColumn('origin');
            $table->dropColumn('nacionality');
            $table->dropColumn('origin_club');
            $table->dropColumn('status');
        });
    }
}
