<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->truncateTables([
        //     'users',
        //     'sports',
        //     'clubs',
        //     'club_sport',
        //     'sport_user',
        //     'team_user'            
        // ]);           
        $this->call(UserSeeder::class);
        $this->call(SportSeeder::class); 
        $this->call(ClubSeeder::class);             

        DB::table('users')
            ->where('id', 1)
            ->update(['club_id' => 1]);

        DB::table('teams')->insert([
            'name' => 'Test',
            'creator_id' => 1,
            'sport_id' => 1,
            'club_id' => 1,
        ]);        

        factory(App\Models\User::class)->create([
            'name' => 'demo',
            'email' => 'demo@demo.com',
            'password' => bcrypt('demo'),
            'club_id' => 1,
            'role' => 'player',
        ]);

        // DB::table('team_user')->insert([
        //     'team_id' => 1,
        //     'user_id' => 1,
        // ]);

        DB::table('team_user')->insert([
            'team_id' => 1,
            'user_id' => 35,
        ]);

        DB::table('sport_user')->insert([
            'sport_id' => 1,
            'user_id' => 35,
        ]);
    }
    protected function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
