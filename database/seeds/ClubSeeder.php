<?php

use App\Models\Club;
use Illuminate\Database\Seeder;

class ClubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Club::class)->create([
            'name' => 'Demo',
            'description' => 'Demo club for testing',
            'creator_id' => 1,
        ]);
    }
}
