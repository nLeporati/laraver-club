<?php

use App\Models\Sport;
use Illuminate\Database\Seeder;

class SportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $professionId = Profession::where('title', 'Desarrollador back-end')->value('id');

        factory(Sport::class)->create([
            'name' => 'Football',
            'description' => 'The football sport',
            'creator_id' => 1,
        ]);

        factory(Sport::class)->create([
            'name' => 'Tennis',
            'description' => 'The Tennis sport',
            'creator_id' => 1,
        ]);

        // factory(Sport::class, 1)->create();
    }
}
