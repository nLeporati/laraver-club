<?php

Route::get('/', function () {
    return view('layout');
});

Route::get('/usuarios', 'UserController@index')
    ->name('users');

Route::get('usuarios/nuevo', 'UserController@create')
    ->name('user.create');

Route::post('/usuarios/nuevo', 'UserController@store')
    ->name('user.store');

Route::get('usuarios/{user}', 'UserController@show')
    ->where('user', '[0-9]+')
    ->name('user.show');

Route::get('usuarios/{user}/editar', 'UserController@edit')
    ->name('user.edit');

Route::put('usuarios/{user}', 'UserController@update')
    ->name('user.update');

Route::delete('usuarios/{user}', 'UserController@destroy')
    ->name('user.destroy');    

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
