<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

require_once "api/users.php";
require_once "api/clubs.php";
require_once "api/sports.php";
require_once "api/teams.php";
require_once "api/works.php";

Route::post('/register', 'AuthController@register');

Route::post('/login', 'AuthController@authenticate');

Route::middleware(['jwt.verify'])->get('/login/hash', 'AuthController@getAuthenticatedUser');

Route::post('/media', 'MediaController@store');
Route::get('/media', 'MediaController@index');

