<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Sports Routes
|--------------------------------------------------------------------------
*/
Route::middleware(['jwt.verify'])->group(function () {
    Route::get('sports', 'SportController@index')
    ->name('sports');

    Route::middleware(['club'])->get('sports/{sport}', 'SportController@show')
    ->where('sport', '[0-9]+')
    ->name('sports.show');

    Route::middleware(['club'])->post('sports', 'SportController@store')
    ->name('sports.store');

    Route::middleware(['club'])->put('sports/{sport}', 'SportController@update')
    ->where('sport', '[0-9]+')
    ->name('sports.update');

    Route::middleware(['club'])->delete('sports/{sport}', 'SportController@destroy')
    ->where('sport', '[0-9]+')
    ->name('sports.destroy');

});