<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Clubs Routes
|--------------------------------------------------------------------------
*/
Route::middleware(['jwt.verify'])->group(function () {
    // index
    Route::get('clubs', 'ClubController@index')
    ->name('clubs');

    // show
    Route::get('clubs/{name}', 'ClubController@show')
    ->name('club.show');

    // update
    Route::middleware(['club'])->put('clubs/{club}', 'ClubController@update')
    ->where('club', '[0-9]+')->name('clubs.update');

    // destroy
    Route::middleware(['club'])->delete('clubs/{club}', 'ClubController@destroy')
    ->where('club', '[0-9]+')->name('clubs.destroy');

    // store
    Route::post('clubs', 'ClubController@store')
    ->name('clubs.store');

});