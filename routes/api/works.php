<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Works Routes
|--------------------------------------------------------------------------
*/
Route::middleware(['jwt.verify'])->group(function () {
    Route::get('works', 'WorkController@index')
    ->name('works');

    Route::middleware(['club'])->get('works/{work}', 'WorkController@show')
    ->where('work', '[0-9]+')
    ->name('works.show');

    Route::post('works', 'WorkController@store')
    ->name('works.store');

    Route::middleware(['club'])->put('works/{work}', 'WorkController@update')
    ->where('Work', '[0-9]+')
    ->name('works.update');

    Route::middleware(['club'])->delete('works/{work}', 'WorkController@destroy')
    ->where('Work', '[0-9]+')
    ->name('works.destroy');

});