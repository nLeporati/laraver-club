<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Teams Routes
|--------------------------------------------------------------------------
*/
Route::middleware(['jwt.verify'])->group(function () {
    Route::get('teams', 'TeamController@index')
    ->name('teams');

    Route::middleware(['club'])->get('teams/{team}', 'TeamController@show')
    ->where('team', '[0-9]+')
    ->name('teams.show');    

    Route::middleware(['club'])->put('teams/{team}', 'TeamController@update')
    ->name('teams.update');

    Route::post('teams', 'TeamController@store')
    ->name('teams.store');

    Route::middleware(['club'])->delete('teams/{team}', 'TeamController@destroy')
    ->where('team', '[0-9]+')
    ->name('teams.destroy');

});