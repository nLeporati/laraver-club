<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
*/
Route::middleware(['jwt.verify'])->group(function () {
    // index 
    Route::get('users', 'UserController@index')
    ->name('users');

    // show
    Route::middleware(['club'])->get('users/{user}', 'UserController@show')
    ->where('user', '[0-9]+')
    ->name('users.show');

    // update
    Route::middleware(['club'])->post('users/{user}', 'UserController@update')
    ->name('users.update');

    // store
    Route::post('users', 'UserController@store')
    ->name('users.store');

    // destroy
    Route::middleware(['club'])->delete('users/{user}', 'UserController@destroy')
    ->name('users.destroy');

    // changeTeam
    Route::middleware(['club'])->post('users/{user}/change-team', 'UserController@changeTeam')
    ->name('users.changeTeam');

    // addTeam
    Route::middleware(['club'])->post('users/{user}/add-team', 'UserController@addTeam')
    ->name('users.addTeam');

    // removeTeam
    Route::middleware(['club'])->post('users/{user}/remove-team', 'UserController@removeTeam')
    ->name('users.removeTeam');

});