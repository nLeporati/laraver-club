@extends('layout')

@section('title', "Usuario #{$user->id}")

@section('content')
<h1>{{ $title }}</h1>
<h3>{{ $user->name }}</h3>
<p>{{ $user->email }}</p>
@if($user->is_admin)
<p>Administrador</p>
@endif

<p>
    <a href="{{ route('user.edit', $user) }}">Editar</a>
</p>
<div>
<form action="{{ route('user.destroy', $user) }}" method="POST">
{{ method_field('DELETE') }}
{{ csrf_field() }}
<button type="submit" class="btn btn-sm btn-danger">Eliminar</button>
</form>
</div>
<p>
    <a href="{{ route('users') }}">Regresar</a>
</p>
            
@endsection