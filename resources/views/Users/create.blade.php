@extends('layout')

@section('title', "Nuevo usuario")

@section('content')
<h1>Crear Usuario</h1>

<form action="{{ url('usuarios/nuevo') }}" method="POST">
  {{ csrf_field() }}
  <div class="form-group">
    <label for="name">Nombre</label>
    <input type="text" class="form-control"  name="name" id="name" placeholder="Complete name">
    @if($errors->has('name'))
      <p>{{ $errors->first('name') }}</p>
    @endif
  </div>  
  <div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" value="{{ old('email') }}">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
  </div>
  <div class="form-check">
    <input type="checkbox" class="form-check-input" name="is_admin" id="is_admin">
    <label class="form-check-label" for="is_admin">Administrador</label>
  </div>
  <br>
  @if($errors->any())
    @foreach($errors->all() as $error)
      <div class="alert alert-danger" role="alert">
        {{ $error }}
      </div>
    @endforeach
  @endif
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
            
@endsection