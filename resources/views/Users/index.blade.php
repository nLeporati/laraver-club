@extends('layout')

@section('title', "Usuarios")

@section('content')
<h1>{{ $title }}</h1>
            
<ul>
    @forelse ($users as $user)              
        <li>
            {{ $user->name }}
            <!-- <a href="{{ url('/usuarios/'.$user->id) }}">detalles</a> -->
            <a href="{{ route('user.show', ['id' => $user->id]) }}">detalles</a>
        </li>
        
    @empty
        <p>No hay usuarios registrados</p>
    @endforelse
</ul>
@endsection