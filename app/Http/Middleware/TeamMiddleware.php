<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class TeamMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $team = Auth::user()->teams()->find($request->get('team_id'));
        if (!isset($team))
        {
            return response()->json(['message'=>'You dont have team authorization'], 401);
        }

        return $next($request);
    }
}
