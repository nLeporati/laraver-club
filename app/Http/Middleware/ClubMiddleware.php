<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

use Closure;

class ClubMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty(Auth::user()->club)) {
            $userClubId = Auth::user()->club->id;

        } else {
            return response()->json(['message'=>'You dont been login'], 401);
        }      

        // Get request club id
        $params = $request->route()->parameters();
        $reqClubId = null;

        if (!empty($params)) {
            foreach ($params as $p) {

                $reqClubId = $p->club_id;

                // Check if search is from a club
                if ($p->getTable() == "clubs") {
                    $reqClubId = $p->id;
                }

                break;
            }
        }

        // Check if its part of the club
        if ($userClubId != $reqClubId) {
            return response()->json(['message'=>'You dont have club authorization'], 401);
        }
        
        return $next($request);
    }
}
