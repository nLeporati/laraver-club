<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClubRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (empty($this->club->id))
        {
            return [
                'name' => 'required|unique:clubs',
                'description' => 'string|max:50',
                'sport_id' => 'required|numeric|exists:sports,id',
                // 'creator_id' => 'required|exists:users,id|numeric',
            ];
        }
        
        if (!empty($this->club->id))
        {
            return [
                'name' => 'unique:clubs,id,'.$this->get('id'),
                'description' => 'string|max:50',
                'creator_id' => 'numeric|exists:users,sid',
                'sport_id' => 'numeric|exists:sports,id',
            ];
        }
    }
}
