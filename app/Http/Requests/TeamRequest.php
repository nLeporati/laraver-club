<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (empty($this->team->id))
        {
            return [
                'name' => 'required|string|unique:clubs,name|max:255',
                'description' => 'required|string|max:255',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'country' => 'string|max:255',
                'city' => 'string|max:255',            
                'status' => 'string|max:50',
                'sport_id' => 'required|exists:sports,id|numeric'
            ];
        }
        
        if (!empty($this->team->id))
        {
            return [
                'name' => 'string|max:255|unique:clubs,id,'.$this->get('id'),
                'description' => 'string|max:255',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'country' => 'string|max:255',
                'city' => 'string|max:255',
                'status' => 'string|max:50',
                'sport_id' => 'exists:sports,id|required|numeric',
                'club_id' => 'exists:clubs,id|numeric'
            ];
        }   
    }
}
