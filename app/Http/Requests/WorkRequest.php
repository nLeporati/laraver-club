<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (empty($this->work->id))
        {
            return [
                'name' => 'required|string|min:4|max:255',
                'description' => 'required|string|max:255',
                'body' => 'required|string|max:255',
                // 'category' => 'required|string',
                'time' => 'numeric',            
                'status' => 'required|string|max:50',
                'sport_id' => 'required|exists:sports,id|numeric'                
            ];
        }
        
        if (!empty($this->work->id))
        {
            return [
                'name' => 'string|min:4|max:255|',
                'description' => 'string|max:255',
                'body' => 'string|max:255',
                // 'category' => 'string',
                'time' => 'numeric',
                'status' => 'string|max:50',
                'sport_id' => 'exists:sports,id|required|numeric'
            ];
        }  
    }
}
