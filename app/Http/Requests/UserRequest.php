<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {       
        if (empty($this->user->id))
        {
            return [
                'run' => 'required|string|max:12',
                'name' => 'required|string|max:255',
                'lastname' => 'required|string|max:255',
                'email' => 'required|email|unique:users',
                'password' => 'required|string',
                // 'image' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                // 'club_id' => 'exists:clubs,id|required|numeric',
                'nacionality' => '',
                'sport_id' => 'exists:sports,id|required|numeric',
                'team_id' => 'sometimes|required|exists:teams,id|numeric',
                'role' => 'required',
            ];
        }
        
        if (!empty($this->user->id))
        {
            return [
                'run' => 'string|max:12',
                'name' => 'string|max:255',
                'lastname' => 'string|max:255',
                'email' => 'email|unique:users,id,'.$this->user->id,
                'password' => 'string',
                // 'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'nacionality' => 'string',
                'club_id' => 'exists:clubs,id|numeric',
                'sport_id' => 'exists:sports,id|numeric',
                'team_id' => 'exists:teams,id|numeric',
                'role' => '',
            ];
        }          
    }
}
