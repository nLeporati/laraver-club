<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (empty($this->team->id))
        {
            return [
                'name' => 'required|unique:sports,id,'.$this->get('id'),
                'description' => 'string|max:50',
                'creator_id' => 'required|numeric'
            ];
        }

        
        if (!empty($this->team->id))
        {
            return [
                'name' => 'unique:sports,id,'.$this->get('id'),
                'description' => 'string|max:50',
                'creator_id' => 'exists:users,id|numeric',
                'club_id' => 'exists:clubs,id|numeric',
            ];
        }
    }
}
