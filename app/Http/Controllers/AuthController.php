<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function authenticate(Request $request)
    {
        // $credentials = $request->only('email', 'password');

        // $credentials = $request->only('json');
        // $credentials = json_decode($credentials['json'],true);

        $credentials = json_decode($request->getContent(), true);
        // dd($credentials);    

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $user = json_decode($request->getContent(), true);

        $validator = Validator::make($user, [            
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string',
            // 'role' => '',
            'sport' => 'required|exists:sports,id|required|numeric',
            // 'run' => 'required_if:role,player|string|max:12',
        ]);

        if($validator->fails()){
                $errors = $validator->errors()->toArray();
                $message = 'The given data was invalid.';
                return response()->json(compact('message','errors'), 400);
        }

        $data = User::create([
            // 'run' => $data,
            'name' => $user['name'],
            'lastname' => $user['lastname'],
            'email' => $user['email'],
            'password' => Hash::make($user['password']),
            'role' => 'user',
        ]);

        $data->sports()->attach($user['sport']);

        $token = JWTAuth::fromUser($data);

        return response()->json(compact('data','token'), 201);
    }

    public function getAuthenticatedUser()
    {        
        try {
            
            $user = JWTAuth::parseToken()->authenticate();
            $data = User::whereId($user->id)->with('club')->first();

        if (empty($user)) {
            return response()->json(['user_not_found'], 404);
        }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('data'));
    }
}
