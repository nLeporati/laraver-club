<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Club;
use App\Http\Resources\TeamResource;
use App\Http\Requests\TeamRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    public function __construct() {
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Team::whereClubId($this->user->club->id)
                    ->simplePaginate(25);

        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        $team = $request->validated();

        $team['creator_id'] = $this->user->id;
        $team['club_id'] = $this->user['club_id'];

        $data = Team::create($team);

        return response()->json(compact('data'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        $data = Team::whereId($team->id)
                    ->whereClubId($this->user->club->id)
                    ->first();

        return response()->json(['data'=>$data], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(TeamRequest $request, Team $team)
    {
        $data = $request->validated();   

        $team->update($data);

        return response()->json(['data'=>$team], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        $team->delete();
        return response()->json(['message'=>'success'], 204);
    }
}
