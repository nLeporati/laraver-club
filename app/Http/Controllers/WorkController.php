<?php

namespace App\Http\Controllers;

use App\Models\Work;
use Illuminate\Http\Request;
use App\Http\Requests\WorkRequest;
use Illuminate\Support\Facades\Auth;

class WorkController extends Controller
{
    public function __construct() {
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Work::whereClubId($this->user->club->id)
                    ->whereSportId(request()->get('sport_id'))
                    ->simplePaginate(25);

        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkRequest $request)
    {
        $work = $request->validated();

        $work['creator_id'] = $this->user->id;
        $work['club_id'] = $this->user['club_id'];

        $data = Work::create($work);

        return response()->json(compact('data'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Work $work)
    {
        $data = Work::whereId($work->id)
                    ->whereClubId($this->user->club->id)
                    ->first();

        return response()->json(compact('data'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkRequest $request, Work $work)
    {
        $data = $request->validated();   

        $work->update($data);

        return response()->json(['data'=>$work], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Work $work)
    {
        $work->delete();
        return response()->json(['message'=>'success'], 204);
    }
}
