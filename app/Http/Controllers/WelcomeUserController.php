<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeUserController extends Controller
{
    public function __invoke($name, $nick=null)
    {
        $name = ucfirst($name);

        if ($nick) {
            return "Usuario: {$name}, nickname: {$nick}";
        } else {
            return "Usuario: {$name}, nickname: sin nickname";
        }
    }
}
