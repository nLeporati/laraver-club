<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $data = $request->all();
        $validator = Validator::make($data, [            
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id'    => 'required',
            'table' => 'required|string',
            'storage'  => 'required|string',
            'prefix'=> 'required|string'
        ]);

        if($validator->fails()){
                $errors = $validator->errors()->toArray();
                $message = 'The given data was invalid.';
                return response()->json(compact('message','errors'), 400);
        }

        $image = upload_image($data['image'], $data['prefix'], $data['storage']);

        DB::table($data['table'])->where('id', $data['id'])->update(['image'=> $image]);

        return response()->json(['data'=>$image], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
