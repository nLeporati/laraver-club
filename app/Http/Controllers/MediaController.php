<?php

namespace App\Http\Controllers;

use App\Models\Media;
use App\Models\Work;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class MediaController extends Controller
{
    public function index()
    {
        $data = Media::where('owner_id', request()->get('id'))
                        ->whereType(request()->get('type'))
                        ->get();

        return response()->json(compact('data'), 200);
    }

    public function store(Request $request)
    {  
        $data = $request->all();
        $validator = Validator::make($data, [            
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id'    => 'required',
            'table' => 'required|string',
            'storage'  => 'required|string',
            'prefix'=> 'required|string',
            'type' => 'required|string|max:10'
        ]);

        if($validator->fails()){
                $errors = $validator->errors()->toArray();
                $message = 'The given data was invalid.';
                return response()->json(compact('message','errors'), 400);
        }

        $image = upload_image($data);

        $media = Media::create([
            'uri' => $data['table'].'/'.$data['storage'].'/'.$image,
            'image' => $image,
            'table' => $data['table'],
            'storage'  => $data['storage'],
            'prefix'=> $data['prefix'],
            'type' => $data['type'],
            'owner_id' => $data['id']
        ]);

        $table = DB::table($data['table'])->where($data['table'].'.id', $data['id'])->first();
        // dd($table);
        // dd(array_key_exists('image', $table));
        if (array_key_exists('image', $table)) {
            DB::table($data['table'])->where('id', $data['id'])->update(['image'=> $image]);
        } 

        return response()->json(['data'=>$image], 200);
    }
}
