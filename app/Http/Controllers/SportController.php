<?php

namespace App\Http\Controllers;

use App\Models\Sport;
use App\Models\Club;
use Illuminate\Http\Request;
use App\Http\Requests\SportRequest;

class SportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(compact(['data'=>Sport::all()]), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SportRequest $request)
    {
        $sport = $request->validated();

        $data = Sport::create($data);

        return response()->json(compact('data'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sport  $sport
     * @return \Illuminate\Http\Response
     */
    public function show(Sport $sport)
    {
        return response()->json(compact(['data'=>$sport]), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sport  $sport
     * @return \Illuminate\Http\Response
     */
    public function update(SportRequest $request, Sport $sport)
    {
        $data = $request->validated();
        $sport->update($data);

        return response()->json(['data'=>$sport], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sport  $sport
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sport $sport)
    {
        $sport->delete();
        return response()->json('success', 204);
    }
}
