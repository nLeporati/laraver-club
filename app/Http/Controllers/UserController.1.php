<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Club;
use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function __construct() {
        $this->user = Auth::user();
        $this->userClub = (!empty($this->user->club) ? $this->user->club->id : 0);
    }

    /**
     * Display a listing of the users by sport and team.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::whereClubId($this->userClub);

        // If send sport
        if (!empty(request()->input('sport'))) {
            $data = $data->whereHas('sports', function($q) {
                        $q->where('id', request()->input('sport'));
                    });
        }

        // If send team
        if (!empty(request()->input('team'))) {
            $data = $data->whereHas('teams', function($q) {
                $q->where('id', request()->input('team'));
            });
        }

        // if send role
        if (!empty(request()->input('role'))) {
            $data = $data->whereRole(request()->input('role'));
        }

        $data = $data->simplePaginate(25);

        return response()->json($data, 200);
    }

    public function show(User $user)
    {
        $data = User::with('teams')->whereId($user->id)
                    ->whereClubId($this->userClub)                    
                    ->first();

        return response()->json(compact('data'), 200);
    }

    public function store(UserRequest $request)
    {
        $user = $request->validated();   

        $user['password'] = bcrypt($user['password']);
        $user['club_id'] = $this->user->club->id;

        if (!empty($user['image'])) {
            $user['image'] = upload_image($user['image'], 'user_image', 'users/avatars/');
        }

        $data = User::create($user);

        $data->sports()->attach($user['sport_id']);
        if (isset($user['team_id'])) $data->teams()->attach($user['team_id']);       

        return response()->json(compact('data'), 201);
    }

    public function update(UserRequest $request, User $user)
    {
        $data = $request->validated();

        if (!empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        }

        // if (!empty($data['image'])) {
        //     $data['image'] = upload_image($data['image'], 'user_image', 'users/avatars/');
        // }

        $user->update($data);

        return response()->json(['data'=>$user], 200);
    }

    public function destroy(User $user)
    {        
        $user->delete();
        return response()->json(['message'=>'success'], 200);
    }

    public function changeTeam(User $user)
    {
        $data = request()->validate([
            'team_id' => 'required|exists:teams,id|numeric']);

        $user->teams()->sync($data['team_id']);

        return response()->json(['message'=>'success'], 200);
    }

    public function addTeam(User $user)
    {
        $data = request()->validate([
            'team_id' => 'required|exists:teams,id|numeric']);

        $user->teams()->syncWithoutDetaching($data['team_id']);

        return response()->json(['message'=>'success'], 200);
    }

    public function removeTeam(User $user)
    {
        $data = request()->validate([
            'team_id' => 'required|exists:teams,id|numeric']);

        $user->teams()->detach($data['team_id']);

        return response()->json(['message'=>'success'], 204);
    }    
        
}
