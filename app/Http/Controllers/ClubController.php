<?php

namespace App\Http\Controllers;

use App\Models\Club;
use App\Models\Sport;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\ClubRequest;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class ClubController extends Controller
{    
    public function __construct() {
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Club::simplePaginate(25);

        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClubRequest $request)
    {  
        $club = $request->validated();

        $club['creator_id'] = $this->user->id; 

        $data = Club::create($club);
        
        $user = $this->user;
        $user['club_id'] = $data->id;
        $user->save();

        return response()->json(['data'=>$data], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function show(string $name)
    {
        $data = Club::whereName($name)->first();        

        return response()->json(compact('data'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function update(ClubRequest $request, Club $club)
    {
        $data = $request->validated();     

        $club->update($data);

        return response()->json(['data'=>$club], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function destroy(Club $club)
    {
        $club->delete();
        return response()->json(['message'=>'success'], 204);
    }
}
