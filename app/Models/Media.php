<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'image', 
        'id',
        'table',
        'storage',
        'prefix',
        'type',
        'uri',
        'owner_id'
    ];
}
