<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'run',
        'username',
        'lastname',
        'cellnumber',
        'image',
        'birthdate',
        'origin',
        'nacionality',
        'origin_club',
        'status',
        'club_id',
        'role'      
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    // protected $casts = [
        
    // ];

    /**
     * The functions for JWT.
     *
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function teams() {
        return $this->belongsToMany(Team::class);
    }

    public function sports() {
        return $this->belongsToMany(Sport::class);
    }
}
