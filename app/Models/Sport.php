<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sport extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'description',
        'image',
        'status',
        'creator_id',
    ];
    
    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function clubs()
    {
        return $this->belongsToMany(Club::class);
    }    

    public function users() {
        return $this->hasMany(User::class);
    }
}
