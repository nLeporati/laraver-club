<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{    
    protected $fillable = [
        'name', 
        'description',
        'image',
        'country',
        'city',
        'status',
        'creator_id',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function sports()
    {
        return $this->belongsToMany(Sport::class);
    }

    public function users() {
        return $this->hasMany(User::class);
    }
}
