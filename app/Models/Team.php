<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name', 
        'description',
        'image',
        'status',
        'creator_id',
        'sport_id',
        'club_id',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function sport()
    {
        return $this->belongsTo(Sport::class);
    }

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function users() {
        return $this->hasMany(User::class);
    }
}
