<?php

if (!function_exists('upload_image')) {
    function upload_image($data){

        $storage_path = 'storage/'.$data['table'].'/'.$data['storage'].'/';
        $extention = $data['image']->getClientOriginalExtension();
        $filename = $data['prefix'].'_'.date("YmdHis").'.'.$extention;

        if (!File::isDirectory(public_path($storage_path))) {
            File::makeDirectory(public_path($storage_path), 0777, true, true);
        }

        Image::make($data['image'])->save(public_path($storage_path . $filename));

        return $filename;
    }
}